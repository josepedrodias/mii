function miiFace(svgId, miiCfg) {
	'use strict';

	var svgEl = document.querySelector('#' + svgId);
	var sB = svgEl.getBoundingClientRect();

	var s = Snap('#' + svgId);

	var S = 512;
	var S2 = S/2;

	function getBBox(g) {
		var b = g.node.getBoundingClientRect();
		console.log(b);

		b = {
			x: b.left - sB.left,
			y: b.top  - sB.top,
			w: b.width,
			h: b.height,
			w2: b.width/2,
			h2: b.height/2
		};

		//b.xc = b.x + b.w2;
		//b.yc = b.y + b.h2;
		b.x0 = b.x - b.w2;
		b.y0 = b.y - b.h2;

		return b;
	}

	Snap.load('./rip/shapes/1-faces/1-shapes/02.svg', function(f) {
		var g = f.select('g');
		var p = f.select('path', g);

		s.append(g);

		//console.log(p);
		/*p.transform(
			Snap.matrix()
				.translate(0, 1.5)
		);*/

		//setTimeout(function() {
			var b = getBBox(g);
			console.log(b);

			var r = s.rect(b.x0, b.y0, b.w, b.h);
			r.attr({stroke:'#FFF', strokeWidth:0.2, fill:'none'});
			g.append(r);

			var maxS = Math.max(b.w, b.h);
			var factor = S / maxS;

			g.transform(
				Snap.matrix()
					//.translate(0, 12)
					.translate(S2, S2)
					.scale(factor)
					//.translate(b.x*factor, b.y*factor)
			);	
		//}, 100);
	});
}
