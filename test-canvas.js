(function() {
	'use strict';

	var c = new fabric.StaticCanvas('c', { // Canvas | StaticCanvas
		width:   800,
		height:  600
		// backgroundColor:   'rgb(100,100,200)',
		// selectionColor:    'blue',
		// selectionLineWidth: 2
	});

	var r = new fabric.Rect({
		left:   100,
		top:    100,
		width:  20,
		height: 20,
		fill:  'red',
		angle:  45,
		opacity: 0.66
	});
	c.add(r);
	r.set({left:20, top:50});
	r.set({strokeWidth:5, stroke:'rgba(100,200,200,0.5)'});
	r.set('angle', 15).set('flipY', true);

	/*fabric.Image.fromURL('mona-lisa.jpg', function(oImg) {
		oImg.set({left:40, angle:10});
		c.add(oImg);
	});*/

	/*var p = new fabric.Path('M 0 0 L 200 100 L 170 200 z');
	p.set({left:120, top:120});
	c.add(p);*/

	fabric.loadSVGFromURL('./rip/shapes/3-eyebrows/02.svg', function(objects, options) {
  		var g = fabric.util.groupSVGElements(objects, options);
  		c.add(g).renderAll();
	});

	c.renderAll();
})();
