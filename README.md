# mii



## credits

This project is heavily inspired in Nintendo Mii characters.

Ripped and converted vector graphics and audio files from
[mii creator flash](http://www.wiiplayable.com/play1/miicreator.swf) [game page](http://www.wiiplayable.com/playgame.php?gameid=157)
using
[JPEXS Free Flash Decompiler](http://www.free-decompiler.com/flash/download/)



## reference

* [fabric](http://fabricjs.com/docs/) used to draw vector graphics on canvas
* [snap.svg](http://snapsvg.io/docs/) to manipulate SVG directly

* [fabric articles](http://fabricjs.com/articles/)
* [snap.svg tutorial](http://snapsvg.io/assets/demos/tutorial/)



## TODO

* miiFace
	* fix render order (it now depends on ajax finish order!)
	* determine transforms for features
	* determine operations on transforms order and deltas

* miiGui
	* export buttons to browser page via snap.svg
	* wire events and sounds
	* manipulate ops with gui
	* I/O
	* read mii config (yeah right)
