function miiFace(canvasId, miiCfg) {
	'use strict';

	var mc = miiCore;

	var c = new fabric.StaticCanvas(canvasId, {
		width:   512,
		height:  512
	});

	function clone(o) {
		return JSON.parse( JSON.stringify(o) );
	}

	function placeSVG(svgPath, transform) {
		fabric.loadSVGFromURL(svgPath, function(objects, options) {
			var g = fabric.util.groupSVGElements(objects, options);
			if (transform) {
				g.set(transform);
			}
			c.add(g).renderAll();
		});
	}

	mc.renderOrder.forEach(function(renderStep) {
		var opt = miiCfg[renderStep];
		//console.log(renderStep, opt);
		if (!opt) { return; }

		var svgPath = [mc.svgPrefixPath, mc.svgDirs[renderStep], '/', opt.shape, '.svg'].join('');

		var transform = {
			scaleX:   8,
			scaleY:   8,
			left:   128,
			top:     64
		};

		if (renderStep === 'faceMakeup') {
			transform.top += 64*4;
		}
		else if (renderStep === 'hair') {
			transform.scaleX = 1.75;
			transform.scaleY = 1.75;
		}
		else if (renderStep === 'eyes' || renderStep === 'eyebrows') {
			transform.scaleX = 2;
			transform.scaleY = 2;
			transform.top  = (renderStep === 'eyes' ? 196 : 164);
			transform.left = 196 - 48;
		}
		else if (renderStep === 'nose') {
			transform.scaleX = 1.75;
			transform.scaleY = 1.75;
			transform.top  = 196;
			transform.left = 256;
		}
		else if (renderStep === 'mouth') {
			transform.scaleX = 1.75;
			transform.scaleY = 1.75;
			transform.top  = 300;
			transform.left = 256;
		}
		else if (renderStep === 'propsBeard') {
			transform.scaleX = 1.75;
			transform.scaleY = 1.75;
			transform.top  = 300;
			transform.left = 256;
		}
		else if (renderStep === 'propsMoustache') {
			transform.scaleX = 1.75;
			transform.scaleY = 1.75;
			transform.top  = 300;
			transform.left = 256;
		}
		else if (renderStep === 'propsGlasses') {
			transform.scaleX = 1.75;
			transform.scaleY = 1.75;
			transform.top  = 300;
			transform.left = 128;
		}
		else if (renderStep === 'propsMole') {
			transform.scaleX = 1.75;
			transform.scaleY = 1.75;
			transform.top  = 300;
			transform.left = 128;
		}

		placeSVG(svgPath, transform);

		/*if (renderStep === 'eyes' || renderStep === 'eyebrows') {
			transform.left = 512 - transform.left;
			//transform.flipX = true;
			transform = clone(transform);
			placeSVG(svgPath, transform, true);
		}*/
	});
}
