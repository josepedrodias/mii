var miiCore = {

	renderOrder: 'faceShape faceMakeup mouth eyes eyebrows propsBeard propsMoustache propsGlasses propsMole hair nose'.split(' '),
	
	svgPrefixPath: './rip/shapes/',
	
	svgDirs: {
		faceShape:      '1-faces/1-shapes',
		faceMakeup:     '1-faces/2-makeup',
		hair:           '2-hairs',
		eyebrows:       '3-eyebrows',
		eyes:           '4-eyes',
		nose:           '5-noses',
		mouth:          '6-mouths',
		propsBeard:     '7-props/1-beards',
		propsMoustache: '7-props/2-moustaches',
		propsGlasses:   '7-props/3-glasses',
		propsMole:      '7-props/4-moles'
	},
	
	operations: {
		faceShape:      {ty:1, sy:1, sx:1},
		faceMakeup:     {ty:1, s:1},
		hair:           {c:1, ty:1, s:1},
		eyebrows:       {c:1, ty:1, r:1, s:1, txx:1},
		eyes:           {r:1, ty:1, s:1, txx:1},
		nose:           {ty:1, s:1},
		mouth:          {ty:1, s:1},
		propsBeard:     {c:1, ty:1, s:1},
		propsMoustache: {c:1, ty:1, s:1},
		propsGlasses:   {c:1, ty:1, s:1},
		propsMole:      {}
	}
};
